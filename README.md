# sigma-cicd-template
A template repository to assist teams in accelerating the development of a Detection Engineering program and detection capabilities utilizing Sigma and their CI/CD platform of choice.

## Usage
1) Clone/fork and keep the CI/CD platform configuration necessary for your pipeline and modify as necessary.
2) Move your rules respository into `rules/` and any relevant configurations for Sigma (CLI/backends/pipelines) into `config/`.

### Initialize
To initialize the repository for the CICD platform of your choice:

```shell
make init-cicd CICD=gitlab
# optionally clean up the .cicd folder once you have what you need
make clean-cicd
make setup
```

### Concourse CI
Install the pipelines using `fly`:

```shell
fly -t "cluster" set-pipeline -p check-rules -c .cicd/concourse/check-rules.yml
```

## Supported CI/CD Platforms
The following are currently available to begin building pipelines with:
- [Bitbucket](https://bitbucket.org/) (cloud/local)
- [Concourse](https://concourse-ci.org/)
- [Earthly](https://earthly.dev/)
- [GitLab](https://about.gitlab.com/) (cloud/local)
- [Jenkins](https://www.jenkins.io/)

## TODO
- [ ] Define appropriate license for project.
- [ ] Add local support for containerized testing.
  - [ ] Docker
  - [ ] Podman
- [ ] Create Makefile or process for aligning configurations/steps between tools (as much as possible).
- [ ] Create pre-commit hook pattern for managing files for rule management.
- [ ] Create process around branch and rule status alignment (if rule status=experimental, force to experimental branch).
  - How to templatize branch protection methods across different platforms?
- [x] Create Makefile step for initializing repository with specific CI/CD configuration.
- [ ] Add initial pipeline/workflow configurations
  - [ ] ArgoCD
  - [x] Concourse
  - [ ] Drone
  - [x] Earthly
  - [x] Jenkins
  - [ ] Woodpecker CI (incl. Forgejo Actions?)
- [ ] Add process for implementing validations with solutions like [Automata](https://github.com/3CORESec/Automata) and [Dettectinator](https://github.com/siriussecurity/dettectinator)
- [ ] Add process for aligning output options/integrations for documentation.
- [ ] Add process for storing rule conversion outputs for individual targets:
- [ ] Add step for publishing rule conversions to wiki (Confluence, etc.) with MITRE ATT&CK map coverage.
- [ ] Add templates for [query post processing](https://blog.sigmahq.io/introducing-query-post-processing-and-output-finalization-to-processing-pipelines-4bfe74087ac1) based on different targets.
- [ ] Add ability to pull/update [rule packages](https://blog.sigmahq.io/introducing-sigma-rule-packages-releases-76043ce42e81) from core sigma rules repository.
  - [ ] Add as Makefile step.
- [ ] Add ability to define any rules repository and compare/pull/update changes.
