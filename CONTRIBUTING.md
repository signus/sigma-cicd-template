# Contributing
I am happy to welcome contributions as this project takes shape!

I ask that for the time being until an alternate method surfaces that all issues and pull requests be done via the [original source](https://codeberg.org/signus/sigma-cicd-template) on Codeberg. The list of mirrors (for visibility) are currently:

- [gitlab.com/signusio/sigma-cicd-template](https://gitlab.com/signusio/sigma-cicd-template)

## Issues
Please create issues [here](https://codeberg.org/signus/sigma-cicd-template/issues) and ensure that:

1. Your problem or proposed feature request is well documented.
2. Your issue is directly related to the needs of the project based on description.
3. Any root issues with Sigma, CI/CD or SIEM tooling themselves are addressed before creating an issue in this project.

## Code Submissions
Please ensure that all submissions are created via a named branch through a [Pull Request](https://codeberg.org/signus/sigma-cicd-template/pulls).

The **main** branch is currently protected as are the following Sigma workflow branches (based on rule status):

- **deprecated**
- **unsupported**
- **testing**
- **experimental**
- **stable**

## TODO

- Add notes around contributions for specific testing workflows for Sigma, SIEM, CI/CD, etc.